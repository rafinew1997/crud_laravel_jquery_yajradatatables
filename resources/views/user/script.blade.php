<script>
    $(function() {
        $('#users-table').DataTable({
            processing: true,
            serverSide: true,
            order : [[ 0, "desc" ]],
            ajax: {
                "url": "/user/json",
                "type": "GET"
            },
            columns: [
                { data: 'id', name: 'id' },
                { data: 'name', name: 'name' },
                { data: 'email', name: 'email' },
                { data: 'created_at', name: 'created_at' },
                {data: 'Actions', name: 'Actions',orderable:false,serachable:false,sClass:'text-center'},
            ]
        });
        
        $('.ModalAddOpen').on('click', function(){
                $('#CreateUserModal').show();
            });
        
        $('.ModalAddClose').on('click', function(){
            $('#CreateUserModal').hide();
        });
        // Create User Ajax request.
        $('#SubmitCreateUserForm').click(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: 'user/store',
                    method: 'post',
                    data: {
                        name: $('#name').val(),
                        email: $('#email').val(),
                        password: $('#password').val(),
                    },
                    success: function(result) {
                        if(result.errors) {
                            $('.alert-danger').html('');
                            $.each(result.errors, function(key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                            });
                        } else if(result) {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.datatable').DataTable().ajax.reload();
                            setTimeout(function(){ 
                                $('.alert-success').hide();
                                $('#CreateUserModal').hide();
                            }, 2000);
                        }
                    }
                });
            });
    
        // Get single User in EditModel
            $('.modelClose').on('click', function(){
                $('#EditUserModal').hide();
            });

            var id;
            $('body').on('click', '#getEditUserData', function(e) {
                // e.preventDefault();
                // $('.alert-danger').html('');
                $('.alert-danger').hide();
                id = $(this).data('id');
                $.ajax({
                    url: "user/"+id+"/edit",
                    method: 'GET',
                    // data: {
                    //     id: id,
                    // },
                    success: function(result) {
                        $('#EditUserModalBody').html(result.html);
                        $('#EditUserModal').show();
                    }
                });
            });
    
           // Update User Ajax request.
           $('#SubmitEditUserForm').click(function(e) {
                e.preventDefault();
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "user/"+id,
                    method: 'PUT',
                    data: {
                        name: $('#editname').val(),
                        email: $('#editemail').val(),
                    },
                    success: function(result) {
                        if(result.errors) {
                            $('.alert-danger').html('');
                            $.each(result.errors, function(key, value) {
                                $('.alert-danger').show();
                                $('.alert-danger').append('<strong><li>'+value+'</li></strong>');
                            });
                        
                        } else {
                            $('.alert-danger').hide();
                            $('.alert-success').show();
                            $('.datatable').DataTable().ajax.reload();
                            setTimeout(function(){ 
                                $('.alert-success').hide();
                                $('#EditUserModal').hide();
                            }, 2000);
                        }
                    }
                });
            });
    
            // Delete User Ajax request.
            var deleteID;
            $('body').on('click', '#getDeleteId', function(){
                deleteID = $(this).data('id');
                $('#DeleteUserModal').show();
            })

            $('#SubmitDeleteUserForm').click(function(e) {
                e.preventDefault();
                var id = deleteID;
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                    }
                });
                $.ajax({
                    url: "user/"+id,
                    method: 'DELETE',
                    success: function(result) {
                        setTimeout(function(){ 
                            $('.datatable').DataTable().ajax.reload();
                            $('#DeleteUserModal').hide();
                        }, 100);
                    }
                });
            }); 
    });
    </script>