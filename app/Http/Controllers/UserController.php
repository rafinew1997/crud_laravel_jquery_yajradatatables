<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use DataTables;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('user.index');
    }

    public function json(){
        return Datatables::of(User::all())
        ->addColumn('Actions', function($data) {
            return '<button type="button" class="btn btn-success btn-sm" id="getEditUserData" data-id="'.$data->id.'">Edit</button>
                <button type="button" data-id="'.$data->id.'" class="btn btn-danger btn-sm" id="getDeleteId">Delete</button>';
        })
        ->rawColumns(['Actions'])
        ->make(true);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // try {
            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
                'password' => 'required|min:6',
            ]);
            
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
            $user = new User;
            $user->storeData($request->all());

            return response()->json(['success'=>'User added successfully']);
        // } catch (\Throwable $th) {
        //     return \response()->json([
        //         'status' =>400,
        //         'message' => $th->getMessage(),
        //     ]);
        // }
       
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        try {
            $user = new User;
            $data = $user->findData($id);

            $html = '<div class="form-group">
                        <label for="Name">Name:</label>
                        <input type="text" class="form-control" name="name" id="editname" value="'.$data->name.'">
                    </div>
                    <div class="form-group">
                        <label for="Email">Email:</label>
                        <textarea class="form-control" name="email" id="editemail">'.$data->email.'                        
                        </textarea>
                    </div>';

            return response()->json(['html'=>$html]);
        } catch (\Throwable $th) {
            return \response()->json([
                'status' =>400,
                'message' => $th->getMessage(),
            ]);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $validator = \Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required',
            ]);
            
            if ($validator->fails()) {
                return response()->json(['errors' => $validator->errors()->all()]);
            }
    
            $user = new User;
            $user->updateData($id, $request->all());
    
            return response()->json(['success'=>'User updated successfully']);
        } catch (\Throwable $th) {
            return \response()->json([
                'status' =>400,
                'message' => $th->getMessage(),
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = new User;
        $user->deleteData($id);

        return response()->json(['success'=>'User deleted successfully']);
    }
}
