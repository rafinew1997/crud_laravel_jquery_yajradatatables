@extends('layouts.app')

@section('content')
    <div class="card-title">
        <button style="float: right; font-weight: 900;" type="button" class="btn btn-info ModalAddOpen" data-dismiss="modal">Tambah User</button>
    </div>
    
    
    <table class="table table-bordered datatable" id="users-table">
        <thead>
            <tr>
                <th>Id</th>
                <th>Name</th>
                <th>Email</th>
                <th>created_at</th>
                <th width="150" class="text-center">Action</th>
            </tr>
        </thead>
    </table>

    @include('user.modal')
  
@stop

@push('scripts')
@include('user.script')
@endpush