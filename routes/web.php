<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\UserController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('user', [UserController::class,'index']);
Route::get('user/{id}/edit', [UserController::class,'edit']);
Route::put('user/{id}', [UserController::class,'update']);
Route::post('user/store', [UserController::class,'store']);
Route::delete('user/{id}', [UserController::class,'destroy']);

// Route::resource('user', UserController::class);
Route::get('user/json', [UserController::class, 'json']);
